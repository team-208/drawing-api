from __future__ import annotations

import enum
from typing import List


class HeadType(enum.Enum):
    ROUND_STYLUS = "ROUND_STYLUS"
    SQUARE_STYLUS = "SQUARE_STYLUS"
    POINT_ERASER = "POINT_ERASER"
    STROKE_ERASER = "STROKE_ERASER"


class Point:

    def __init__(self, x: int, y: int):
        self.x: int = x
        self.y: int = y

    class Factory:
        @staticmethod
        def from_dictionary(point_dictionary: dict) -> Point:
            x: int = point_dictionary.get("x")
            y: int = point_dictionary.get("y")

            return Point(x, y)


class Stroke:

    def __init__(self, points: List[Point], color: str, head_type: str, size: int, z_index: int):
        self.points: List[Point] = points
        self.color: str = color
        self.head_type: str = head_type
        self.size: int = size
        self.z_index: int = z_index

    class Factory:
        @staticmethod
        def from_dictionary(stroke_dictionary: dict) -> Stroke:
            color: str = stroke_dictionary.get("color")
            head_type: str = stroke_dictionary.get("head_type")
            size: int = stroke_dictionary.get("size")
            z_index: int = stroke_dictionary.get("z_index")

            points: List[Point] = list()

            for dict_point in stroke_dictionary.get("points"):
                points.append(Point.Factory.from_dictionary(dict_point))

            return Stroke(points=points, color=color, head_type=head_type, size=size, z_index=z_index)


class Drawing:

    def __init__(self, strokes: List[Stroke], word: str = ""):
        self.word: str = word
        self.strokes: List[Stroke] = strokes

    class Factory:
        @staticmethod
        def from_dictionary(drawing_dictionary: dict) -> Drawing:
            strokes: List[Stroke] = list()

            for dict_stroke in drawing_dictionary.get("strokes"):
                strokes.append(Stroke.Factory.from_dictionary(dict_stroke))

            return Drawing(word=drawing_dictionary.get("word", ""), strokes=strokes)
