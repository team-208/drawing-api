import math
import random
from typing import List

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import CreateAPIView

from core.models import Drawing, Stroke, Point, HeadType
from core.utils import to_dict


class StrokeOrderer:

    @staticmethod
    def _filter_by_head_type(strokes: List[Stroke], head_type: HeadType):
        return [stroke for stroke in strokes if stroke.head_type == head_type.name]

    @staticmethod
    def shuffle_with_eraser_priority(strokes_to_shuffle: List[Stroke]):

        random.shuffle(strokes_to_shuffle)

        stroke_erasers: List[Stroke] = StrokeOrderer._filter_by_head_type(strokes_to_shuffle, HeadType.STROKE_ERASER)
        point_erasers: List[Stroke] = StrokeOrderer._filter_by_head_type(strokes_to_shuffle, HeadType.POINT_ERASER)

        if len(point_erasers) > 0 and len(stroke_erasers) > 0:

            StrokeOrderer.push_stroke_erasers_to_end(strokes_to_shuffle)

            last_point_eraser_index: int = max(
                index for index, value
                in enumerate(strokes_to_shuffle)
                if value.head_type == HeadType.POINT_ERASER.name
            )

            end_slice = strokes_to_shuffle[last_point_eraser_index + 1:]
            random.shuffle(end_slice)
            strokes_to_shuffle[last_point_eraser_index + 1:] = end_slice

    @staticmethod
    def push_stroke_erasers_to_end(strokes: List[Stroke]):

        stroke_erasers: List[Stroke] = StrokeOrderer._filter_by_head_type(strokes, HeadType.STROKE_ERASER)

        for eraser in stroke_erasers:
            strokes.remove(eraser)
            strokes.append(eraser)


class RandomTransform(CreateAPIView):

    def post(self, request, *args, **kwargs):

        drawing: Drawing = Drawing.Factory.from_dictionary(request.data)

        StrokeOrderer.shuffle_with_eraser_priority(drawing.strokes)

        for stroke in drawing.strokes:
            is_changing_direction: bool = bool(random.getrandbits(1))

            if is_changing_direction:
                stroke.points.reverse()

        return Response(to_dict(drawing), status=status.HTTP_200_OK)


class PanoramicTransform(CreateAPIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.modes: dict = {
            "left": self._calculate_min_distance_from_left_side,
            "top": self._calculate_min_distance_from_top_side,
            "right": self._calculate_min_distance_from_right_side,
            "bottom": self._calculate_min_distance_from_bottom_side,
        }

    def post(self, request: Request, *args, **kwargs):

        mode = self.modes.get(request.query_params.get("mode"), self._calculate_min_distance_from_left_side)

        drawing: Drawing = Drawing.Factory.from_dictionary(request.data)

        strokes_distances: List[tuple] = list()

        for stroke in drawing.strokes:
            min_distance_from_side: int = mode(stroke)
            strokes_distances.append((min_distance_from_side, stroke))

        strokes_distances = sorted(strokes_distances, key=lambda t: t[0])

        new_strokes: List[Stroke] = [p[1] for p in strokes_distances]

        drawing.strokes = new_strokes

        StrokeOrderer.push_stroke_erasers_to_end(drawing.strokes)

        return Response(to_dict(drawing), status=status.HTTP_200_OK)

    @staticmethod
    def _calculate_min_distance_from_left_side(stroke: Stroke) -> int:

        if stroke.points[0].x > stroke.points[-1].x:
            stroke.points.reverse()

        closest_point_to_side: Point = min(stroke.points, key=lambda point: point.x)
        return closest_point_to_side.x

    @staticmethod
    def _calculate_min_distance_from_right_side(stroke: Stroke) -> int:

        if stroke.points[-1].x > stroke.points[0].x:
            stroke.points.reverse()

        closest_point_to_side: Point = max(stroke.points, key=lambda point: point.x)
        return -closest_point_to_side.x

    @staticmethod
    def _calculate_min_distance_from_top_side(stroke: Stroke) -> int:

        if stroke.points[0].y > stroke.points[-1].y:
            stroke.points.reverse()

        closest_point_to_side: Point = min(stroke.points, key=lambda point: point.y)
        return closest_point_to_side.y

    @staticmethod
    def _calculate_min_distance_from_bottom_side(stroke: Stroke) -> int:

        if stroke.points[-1].y > stroke.points[0].y:
            stroke.points.reverse()

        closest_point_to_side: Point = max(stroke.points, key=lambda point: point.y)
        return -closest_point_to_side.y


class CenteredTransform(CreateAPIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.modes: dict = {
            "inwards": self._calculate_max_distance_from_center,
            "outwards": self._calculate_min_distance_from_center,
        }

    def post(self, request, *args, **kwargs):

        mode = self.modes.get(request.query_params.get("mode"), self._calculate_min_distance_from_center)

        drawing: Drawing = Drawing.Factory.from_dictionary(request.data)

        strokes_distances: List[tuple] = list()

        center: Point = self._find_center_of_drawing(drawing.strokes)

        for stroke in drawing.strokes:
            distance_from_center: float = mode(stroke, center)
            strokes_distances.append((distance_from_center, stroke))

        strokes_distances = sorted(strokes_distances, key=lambda t: t[0])

        new_strokes: List[Stroke] = [p[1] for p in strokes_distances]

        drawing.strokes = new_strokes

        StrokeOrderer.push_stroke_erasers_to_end(drawing.strokes)

        return Response(to_dict(drawing), status=status.HTTP_200_OK)

    def _calculate_min_distance_from_center(self, stroke: Stroke, center: Point) -> float:
        first_point_to_center: float = self._distance_between(stroke.points[0], center)
        last_point_to_center: float = self._distance_between(stroke.points[-1], center)

        if first_point_to_center > last_point_to_center:
            stroke.points.reverse()

        closest_point_to_center: Point = min(stroke.points, key=lambda point: self._distance_between(point, center))
        return self._distance_between(closest_point_to_center, center)

    def _calculate_max_distance_from_center(self, stroke: Stroke, center: Point) -> float:
        first_point_to_center: float = self._distance_between(stroke.points[0], center)
        last_point_to_center: float = self._distance_between(stroke.points[-1], center)

        if last_point_to_center > first_point_to_center:
            stroke.points.reverse()

        closest_point_to_center: Point = max(stroke.points, key=lambda point: self._distance_between(point, center))
        return -self._distance_between(closest_point_to_center, center)

    def _find_center_of_drawing(self, strokes: List[Stroke]) -> Point:

        all_x: List[int] = self.flatten_list_of_lists(map(lambda stroke: [point.x for point in stroke.points], strokes))
        all_y: List[int] = self.flatten_list_of_lists(map(lambda stroke: [point.y for point in stroke.points], strokes))

        min_x = min(all_x)
        min_y = min(all_y)
        max_x = max(all_x)
        max_y = max(all_y)

        middle_x = int(min_x + (max_x - min_x) / 2)
        middle_y = int(min_y + (max_y - min_y) / 2)

        return Point(middle_x, middle_y)

    @staticmethod
    def flatten_list_of_lists(list_of_lists):
        return [item for sublist in list_of_lists for item in sublist]

    @staticmethod
    def _distance_between(point_a: Point, point_b: Point) -> float:
        return math.sqrt(((point_b.x - point_a.x) ** 2) + ((point_b.y - point_a.y) ** 2))
