from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response

from convert.converter import Converter
from convert.serializers import ImageSerializer
from core.utils import to_dict


class ConvertPng(CreateAPIView):
    serializer_class = ImageSerializer
    parser_class = (FileUploadParser,)
    converter = Converter()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        drawing = self.converter.convert_image(request.data['image'])
        return Response(to_dict(drawing), status=status.HTTP_200_OK)

