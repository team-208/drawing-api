from django.urls import path

from convert.views import ConvertPng

urlpatterns = [
    path('png', ConvertPng.as_view()),
]
