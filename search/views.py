import json
import random
from enum import Enum
from os import path
from threading import Thread
from typing import List

from rest_framework import status

from rest_framework.generics import RetrieveAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from quickdraw import QuickDrawData, QuickDrawing

from core.models import Drawing, Stroke, Point, HeadType
from core.utils import to_json_string, to_dict
from search.wordbank import quickdraw_words

PREPROCESSED_DATA_FILEPATH: str = "search/preprocesseddata.json"


class QuickDrawSearch(RetrieveAPIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not path.exists(PREPROCESSED_DATA_FILEPATH):
            self._update_preprocessed_drawing()

    def get(self, request: Request, *args, **kwargs):

        excluded_word: str = request.query_params.get("exclude")

        with open(PREPROCESSED_DATA_FILEPATH, "r") as data_file:
            drawing_in_file = data_file.read()

        drawing_response = json.loads(drawing_in_file)
        is_same_word: bool = excluded_word and drawing_response.get("word") == excluded_word

        if is_same_word:  # if it's the same word, we compute another one on the spot
            drawing_response = self._update_preprocessed_drawing()
        else:  # if it isn't, we can then send back the preprocessed drawing and compute another one afterwards
            Thread(target=self._update_preprocessed_drawing, args=()).start()

        return Response(drawing_response, status=status.HTTP_200_OK)

    def _update_preprocessed_drawing(self):
        drawing: Drawing = self._get_random_drawing()
        drawing_json = to_json_string(drawing)

        with open(PREPROCESSED_DATA_FILEPATH, "w+") as data_file:
            data_file.write(drawing_json)

        return to_dict(drawing)

    @staticmethod
    def _get_random_drawing() -> Drawing:
        quick_draw = QuickDrawData()
        quickdraw_drawing: QuickDrawing = quick_draw.get_drawing(random.choice(quickdraw_words))

        return QuickDrawSearch._parse_to_drawing(quickdraw_drawing)

    @staticmethod
    def _parse_to_drawing(quickdraw_drawing: QuickDrawing) -> Drawing:
        width: int = 255
        height: int = 255
        multiplier: int = 100000
        padding_x: int = 260
        padding_y: int = 30

        strokes: List[Stroke] = list()

        for index, stroke in enumerate(quickdraw_drawing.strokes):
            points: List[Point] = list()
            for point in stroke:

                # ajusts the quickdraw square image to a rectangle screen ratio
                x: int = ((point[0] + padding_x // 2) * multiplier // (width + padding_x))
                y: int = ((point[1] + padding_y // 2) * multiplier // (height + padding_y))

                points.append(Point(x, y))

            # magic color, magic head_type and magic size TO BE IMPLEMENTED in the future
            new_stroke: Stroke = Stroke(
                points=points,
                color="#000000",
                head_type=HeadType.ROUND_STYLUS.value,
                size=1,
                z_index=-index
            )
            strokes.append(new_stroke)

        return Drawing(strokes=strokes, word=quickdraw_drawing.name)
