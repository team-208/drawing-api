from django.core.files.uploadedfile import InMemoryUploadedFile

from core.models import Drawing, Stroke, Point


class Converter:

    def convert_image(self, image: InMemoryUploadedFile) -> Drawing:
        with open(image.name, 'wb') as new_image:
            for file_bytes in image.chunks(chunk_size=1024):
                new_image.write(file_bytes)

        new_image.close()

        return Drawing([Stroke([Point(1, 1)], 3, 5, "0x00")])

