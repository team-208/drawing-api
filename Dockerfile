FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

COPY requirements.txt .

RUN apk add --no-cache --virtual .build-deps \
    ca-certificates gcc linux-headers musl-dev jpeg-dev zlib-dev

RUN python3 -m pip install -r requirements.txt --no-cache-dir

COPY . .

EXPOSE 9000
CMD ["python", "manage.py", "runserver", "0.0.0.0:9000"]
