import json


def to_dict(instance):
    return json.loads(to_json_string(instance))


def to_json_string(instance):
    return json.dumps(instance, default=lambda o: getattr(o, '__dict__', str(o)), indent=4)
