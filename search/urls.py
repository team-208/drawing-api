from django.urls import path

from search.views import QuickDrawSearch

urlpatterns = [
    path('', QuickDrawSearch.as_view()),
]
