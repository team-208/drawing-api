from django.urls import path

from transform.views import RandomTransform, PanoramicTransform, CenteredTransform

urlpatterns = [
    path('random', RandomTransform.as_view()),
    path('panoramic', PanoramicTransform.as_view()),
    path('centered', CenteredTransform.as_view()),
]
